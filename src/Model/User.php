<?php

namespace AshleyDawson\SimpleBlog\Model;

use AshleyDawson\SimpleFramework\Persistence\Model\AbstractModel;

/**
 * Class User
 *
 * @package AshleyDawson\SimpleBlog\Model
 */
class User extends AbstractModel
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $salt;
}