<?php

namespace AshleyDawson\SimpleBlog\Model;

use AshleyDawson\SimpleFramework\Persistence\Model\AbstractModel;

/**
 * Class Post
 *
 * @package AshleyDawson\SimpleBlog\Model
 */
class Post extends AbstractModel
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $body;

    /**
     * @var \DateTime (Must have property name like *At for transformations)
     */
    public $createdAt;
}