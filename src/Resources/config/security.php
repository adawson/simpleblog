<?php

/**
 * Security firewall configuration
 */
return array(

    // Require authentic tokens to access "/admin" section of blog
    'path_prefix' => '/admin',
    'login_route' => 'user_login',
    'login_process_route' => 'user_login_process',
    'logout_route' => 'user_logout',

);