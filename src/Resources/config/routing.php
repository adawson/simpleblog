<?php

return array(

    // Homepage (List of posts)
    'homepage' => array(
        'path' => '/',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'posts_controller',
            '_action' => 'list',
        ),
    ),

    // View single post
    'view_post' => array(
        'path' => '/posts/{id}',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'posts_controller',
            '_action' => 'view',
        ),
    ),

    // Admin homepage (List of posts)
    'admin_homepage' => array(
        'path' => '/admin',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'admin_posts_controller',
            '_action' => 'list',
        ),
    ),

    // Admin add new post form
    'admin_add_post_form' => array(
        'path' => '/admin/posts/add',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'admin_posts_controller',
            '_action' => 'add',
        ),
    ),

    // Admin add new post form
    'admin_add_post' => array(
        'path' => '/admin/posts/add',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_POST,
        'defaults' => array(
            '_controller' => 'admin_posts_controller',
            '_action' => 'performAdd',
        ),
    ),

    // Admin edit post form
    'admin_edit_post_form' => array(
        'path' => '/admin/posts/{id}',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'admin_posts_controller',
            '_action' => 'edit',
        ),
    ),

    // Admin edit post form
    'admin_edit_post' => array(
        'path' => '/admin/posts/{id}/edit',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_POST,
        'defaults' => array(
            '_controller' => 'admin_posts_controller',
            '_action' => 'performEdit',
        ),
    ),

    // Admin edit single post
    'admin_delete_post' => array(
        'path' => '/admin/posts/{id}/delete',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'admin_posts_controller',
            '_action' => 'delete',
        ),
    ),

    // Admin user login
    'user_login' => array(
        'path' => '/admin/login',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'user_controller',
            '_action' => 'login',
        ),
    ),

    // Admin user login process
    'user_login_process' => array(
        'path' => '/admin/login',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_POST,
        'defaults' => array(
            '_controller' => 'user_controller',
            '_action' => 'processLogin',
        ),
    ),

    // Admin user logout
    'user_logout' => array(
        'path' => '/admin/logout',
        'method' => \AshleyDawson\SimpleFramework\Http\RequestInterface::METHOD_GET,
        'defaults' => array(
            '_controller' => 'user_controller',
            '_action' => 'logout',
        ),
    ),

);