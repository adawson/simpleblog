<?php $this->extends_template('frontend-layout.html.php') ?>

<?php $this->block('body') ?>

    <h2>Latest Posts</h2>

    <?php foreach ($posts as $post): ?>
        <article>
            <header>
                <h3><a href="<?php echo $this->relative_path('view_post', array('id' => $post->id)) ?>"><?php echo htmlspecialchars($post->title) ?></a></h3>
                <strong>
                    <time datetime="<?php echo $post->createdAt->format('Y-m-d H:i:s') ?>"><?php echo $post->createdAt->format('l jS F, Y \a\t g:ia') ?></time>
                </strong>
            </header>
            <?php echo nl2br($this->truncate(htmlspecialchars($post->body), 100) . '&hellip;') ?>
        </article>
        <hr />
    <?php endforeach ?>

<?php $this->end_block() ?>