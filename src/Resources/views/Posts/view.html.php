<?php $this->extends_template('frontend-layout.html.php') ?>

<?php $this->block('page_title') ?>
    <?php echo htmlspecialchars($post->title) ?> | Frank Recruitment Group Blog
<?php $this->end_block() ?>

<?php $this->block('body') ?>

    <article>
        <header>
            <h2><?php echo htmlspecialchars($post->title) ?></h2>
            <strong>
                <time datetime="<?php echo $post->createdAt->format('Y-m-d H:i:s') ?>"><?php echo $post->createdAt->format('l jS F, Y \a\t g:ia') ?></time>
            </strong>
        </header>
        <?php echo nl2br(htmlspecialchars($post->body)) ?>
    </article>

    <div>
        <a href="<?php echo $this->relative_path('homepage') ?>">&laquo; Back to List</a>
    </div>

<?php $this->end_block() ?>