<!doctype html>
<html>
    <head>
        <title>Admin | Simple Blog</title>
    </head>
    <body>

        <?php $this->include_template('Default/user-menu.html.php') ?>

        <header>
            <h1><a href="<?php echo $this->relative_path('admin_homepage') ?>">Simple Blog</a></h1>
        </header>

        <nav>
            <ul>
                <li><a href="<?php echo $this->relative_path('homepage') ?>">View Blog</a></li>
                <li><a href="<?php echo $this->relative_path('admin_homepage') ?>">List Posts</a></li>
                <li><a href="<?php echo $this->relative_path('admin_add_post_form') ?>">New Post</a></li>
            </ul>
        </nav>

        <main>
            <?php echo $this->get_block('body') ?>
        </main>

        <?php $this->include_template('Default/footer.html.php') ?>

    </body>
</html>