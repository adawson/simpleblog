<?php $this->extends_template('frontend-layout.html.php') ?>

<?php $this->block('body') ?>

    <h2>Login</h2>

    <?php if (isset($loginFailed) && $loginFailed): ?>
        <p>
            Login failed, bad credentials
        </p>
    <?php endif ?>

    <form action="<?php echo $this->relative_path('user_login_process') ?>" method="post">
        <fieldset>

            <legend>Login Form</legend>

            <div>
                <label for="email">Email: </label>
                <input type="email" name="email" id="email" />
            </div>

            <div>
                <label for="password">Password: </label>
                <input type="password" name="password" id="password" />
            </div>

            <div>
                <button type="submit">Login</button>
            </div>

        </fieldset>
    </form>

<?php $this->end_block() ?>