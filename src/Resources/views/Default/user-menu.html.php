
<?php if ($this->current_token() && $this->current_token()->isAuthenticated()): ?>
    <div>
        Welcome back | <a href="<?php echo $this->relative_path('admin_homepage') ?>">Admin</a> | <a href="<?php echo $this->relative_path('user_logout') ?>">Logout</a>
    </div>
<?php else: ?>
    <div>
        <a href="<?php echo $this->relative_path('user_login') ?>">Login</a>
    </div>
<?php endif ?>
