<?php $this->extends_template('admin-layout.html.php') ?>

<?php $violations = isset($violations) ? $violations : array() ?>

<?php $this->block('body') ?>

    <h2>New Post</h2>

    <form action="<?php echo $this->relative_path('admin_add_post') ?>" method="post">
        <fieldset>

            <legend>Post Form</legend>

            <div>
                <label for="title">Title: </label>
                <?php echo $this->list_validation_messages($violations, 'title') ?>
                <input type="text" name="title" id="title" maxlength="50" value="<?php echo isset($post) ? $post->title : '' ?>" />
            </div>

            <div>
                <label for="body">Body: </label>
                <?php echo $this->list_validation_messages($violations, 'body') ?>
                <textarea name="body" id="body"><?php echo isset($post) ? $post->body : '' ?></textarea>
            </div>

            <div>
                <button type="submit">Create New Post</button>
                <a href="<?php echo $this->relative_path('admin_homepage') ?>">Cancel</a>
            </div>

        </fieldset>
    </form>

<?php $this->end_block() ?>