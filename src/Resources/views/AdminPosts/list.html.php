<?php $this->extends_template('admin-layout.html.php') ?>

<?php $this->block('body') ?>

    <h2>Posts</h2>

    <table cellpadding="5" border="1">
        <thead>
            <tr>
                <th>Title</th>
                <th>Created At</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $post): ?>
                <tr>
                    <td>
                        <a href="<?php echo $this->relative_path('admin_edit_post_form', array('id' => $post->id)) ?>">
                            <?php echo $post->title ?>
                        </a>
                    </td>
                    <td><?php echo $post->createdAt->format('l jS F, Y \a\t g:ia') ?></td>
                    <td>
                        <a href="<?php echo $this->relative_path('admin_edit_post_form', array('id' => $post->id)) ?>">Edit</a> |
                        <a href="<?php echo $this->relative_path('admin_delete_post', array('id' => $post->id)) ?>" onclick="return confirm('Are you sure?');">Delete</a>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

<?php $this->end_block() ?>