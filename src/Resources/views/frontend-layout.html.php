<!doctype html>
<html>
    <head>
        <title><?php echo $this->get_block('page_title') ?: 'Simple Blog' ?></title>
    </head>
    <body>

        <?php $this->include_template('Default/user-menu.html.php') ?>

        <header>
            <h1><a href="<?php echo $this->relative_path('homepage') ?>">Simple Blog</a></h1>
        </header>

        <main>
            <?php echo $this->get_block('body') ?>
        </main>

        <?php $this->include_template('Default/footer.html.php') ?>

    </body>
</html>