<?php

namespace AshleyDawson\SimpleBlog;

use AshleyDawson\SimpleBlog\Controller\AdminPostsController;
use AshleyDawson\SimpleBlog\Controller\PostsController;
use AshleyDawson\SimpleBlog\Controller\UserController;
use AshleyDawson\SimpleBlog\Validation\PostValidation;
use AshleyDawson\SimpleFramework\AbstractApplication;
use AshleyDawson\SimpleFramework\DependencyInjection\ContainerInterface;

/**
 * Class Application
 *
 * @package AshleyDawson\SimpleBlog
 */
class Application extends AbstractApplication
{
    /**
     * {@inheritdoc}
     */
    protected function configureContainer()
    {
        // Parameters
        $this->setParameter('template_base_paths', array(
            __DIR__ . DIR_SEP . 'Resources' . DIR_SEP . 'views',
        ));

        // Register Validators
        $this->register('validation.post', function (ContainerInterface $c) {
            return new PostValidation();
        });

        // Register Controllers
        $this->register('posts_controller', function (ContainerInterface $c) {
            return new PostsController(
                $c->get('request'),
                $c->get('templating'),
                $c->get('persistence_manager.default')
            );
        });

        $this->register('admin_posts_controller', function (ContainerInterface $c) {
            return new AdminPostsController(
                $c->get('request'),
                $c->get('templating'),
                $c->get('persistence_manager.default'),
                $c->get('routing'),
                $c->get('validation.post')
            );
        });

        $this->register('user_controller', function (ContainerInterface $c) {
            return new UserController(
                $c->get('request'),
                $c->get('templating'),
                $c->get('persistence_manager.default'),
                $c->get('security'),
                $c->get('routing')
            );
        });
    }

    /**
     * {@inheritdoc}
     */
    protected function getConfigParametersPath()
    {
        return __DIR__ . DIR_SEP . '..' . DIR_SEP . 'etc' . DIR_SEP . 'parameters.php';
    }

    /**
     * {@inheritdoc}
     */
    protected function getRoutingConfigPath()
    {
        return __DIR__ . DIR_SEP . 'Resources' . DIR_SEP . 'config' . DIR_SEP . 'routing.php';
    }

    /**
     * {@inheritdoc}
     */
    protected function getSecurityConfigPath()
    {
        return __DIR__ . DIR_SEP . 'Resources' . DIR_SEP . 'config' . DIR_SEP . 'security.php';
    }
}