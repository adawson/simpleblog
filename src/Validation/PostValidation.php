<?php

namespace AshleyDawson\SimpleBlog\Validation;

use AshleyDawson\SimpleFramework\Validation\AbstractValidation;

/**
 * Class PostValidation
 *
 * @package AshleyDawson\SimpleBlog\Validation
 */
class PostValidation extends AbstractValidation
{
    /**
     * {@inheritdoc}
     */
    public function getRules()
    {
        return array(

            // Title rules
            'title' => array(
                array(
                    'message' => 'Please provide a title',
                    'validator' => function ($value) {
                        return ( ! empty($value));
                    },
                ),
                array(
                    'message' => 'Title must be 50 characters or less',
                    'validator' => function ($value) {
                        return (strlen($value) <= 50);
                    },
                ),
            ),

            // Body rules
            'body' => array(
                array(
                    'message' => 'Please provide a body',
                    'validator' => function ($value) {
                        return ( ! empty($value));
                    },
                ),
            ),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isModelSupported($model)
    {
        return ($model instanceof \AshleyDawson\SimpleBlog\Model\Post);
    }
}