<?php

namespace AshleyDawson\SimpleBlog\Controller;

use AshleyDawson\SimpleBlog\Model\Post;
use AshleyDawson\SimpleFramework\Http\RedirectResponse;
use AshleyDawson\SimpleFramework\Http\RequestInterface;
use AshleyDawson\SimpleFramework\Http\Response;
use AshleyDawson\SimpleFramework\Persistence\PersistenceManagerInterface;
use AshleyDawson\SimpleFramework\Routing\RoutingInterface;
use AshleyDawson\SimpleFramework\Templating\TemplatingInterface;
use AshleyDawson\SimpleFramework\Validation\Exception\ValidationViolationException;
use AshleyDawson\SimpleFramework\Validation\ValidationInterface;

/**
 * Class AdminPostsController
 *
 * @package AshleyDawson\SimpleBlog\Controller
 */
class AdminPostsController
{
    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @var TemplatingInterface
     */
    private $_templating;

    /**
     * @var PersistenceManagerInterface
     */
    private $_persistenceManager;

    /**
     * @var RoutingInterface
     */
    private $_routing;

    /**
     * @var ValidationInterface
     */
    private $_postValidation;

    /**
     * Constructor
     *
     * @param RequestInterface $request
     * @param TemplatingInterface $templating
     * @param PersistenceManagerInterface $persistenceManager
     * @param RoutingInterface $routing
     * @param ValidationInterface $postValidation
     */
    public function __construct(
        RequestInterface $request,
        TemplatingInterface $templating,
        PersistenceManagerInterface $persistenceManager,
        RoutingInterface $routing,
        ValidationInterface $postValidation
    ) {
        $this->_request = $request;
        $this->_templating = $templating;
        $this->_persistenceManager = $persistenceManager;
        $this->_routing = $routing;
        $this->_postValidation = $postValidation;
    }

    /**
     * List posts action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function listAction()
    {
        // Get all posts, sorted in descending chronological order
        $posts = $this->_persistenceManager->findAll(
            'AshleyDawson\SimpleBlog\Model\Post',
            array(
                'createdAt' => 'desc',
            )
        );

        // Return the response
        return new Response(
            $this->_templating->render('AdminPosts/list.html.php', array(
                'posts' => $posts,
            ))
        );
    }

    /**
     * Add post form action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function addAction()
    {
        return new Response(
            $this->_templating->render('AdminPosts/add.html.php')
        );
    }

    /**
     * Perform add new post
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function performAddAction()
    {
        $post = new Post();

        // Hydrate new post
        $post->title = $this->_request->getRequestParameter('title');
        $post->body = $this->_request->getRequestParameter('body');
        $post->createdAt = new \DateTime('now');

        // Validate post
        try {
            $this->_postValidation->validate($post);
        }
        catch (ValidationViolationException $e) {
            return new Response(
                $this->_templating->render('AdminPosts/add.html.php', array(
                    'violations' => $e->getViolations(),
                    'post' => $post
                )),
                400
            );
        }

        // Persist new post
        $this->_persistenceManager->persist($post);

        // Redirect to list
        return new RedirectResponse(
            $this->_routing->getRoute('admin_homepage')->getPath()
        );
    }

    /**
     * Edit post form action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function editAction()
    {
        $id = $this->_request->getQueryParameter('id');

        // Try to get the post by its PK
        $post = $this->_persistenceManager->find('AshleyDawson\SimpleBlog\Model\Post', $id);

        return new Response(
            $this->_templating->render('AdminPosts/edit.html.php', array(
                'post' => $post
            ))
        );
    }

    /**
     * Perform edit post
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function performEditAction()
    {
        $id = $this->_request->getQueryParameter('id');

        // Try to get the post by its PK
        $post = $this->_persistenceManager->find('AshleyDawson\SimpleBlog\Model\Post', $id);

        // Hydrate existing post
        $post->title = $this->_request->getRequestParameter('title');
        $post->body = $this->_request->getRequestParameter('body');

        // Validate post
        try {
            $this->_postValidation->validate($post);
        }
        catch (ValidationViolationException $e) {
            return new Response(
                $this->_templating->render('AdminPosts/edit.html.php', array(
                    'post' => $post,
                    'violations' => $e->getViolations(),
                )),
                400
            );
        }

        // Update existing post
        $this->_persistenceManager->update($post);

        // Redirect to list
        return new RedirectResponse(
            $this->_routing->getRoute('admin_homepage')->getPath()
        );
    }

    /**
     * Delete post action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function deleteAction()
    {
        $id = $this->_request->getQueryParameter('id');

        $this->_persistenceManager->delete('AshleyDawson\SimpleBlog\Model\Post', $id);

        return new RedirectResponse(
            $this->_routing->getRoute('admin_homepage')->getPath()
        );
    }
}