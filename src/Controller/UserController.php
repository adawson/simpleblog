<?php

namespace AshleyDawson\SimpleBlog\Controller;

use AshleyDawson\SimpleBlog\Model\User;
use AshleyDawson\SimpleFramework\Http\RedirectResponse;
use AshleyDawson\SimpleFramework\Http\RequestInterface;
use AshleyDawson\SimpleFramework\Http\Response;
use AshleyDawson\SimpleFramework\Persistence\PersistenceManagerInterface;
use AshleyDawson\SimpleFramework\Security\SecurityInterface;
use AshleyDawson\SimpleFramework\Security\SimpleAuthenticationToken;
use AshleyDawson\SimpleFramework\Templating\TemplatingInterface;
use AshleyDawson\SimpleFramework\Routing\RoutingInterface;

/**
 * Class UserController
 *
 * @package AshleyDawson\SimpleBlog\Controller
 */
class UserController
{
    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @var TemplatingInterface
     */
    private $_templating;

    /**
     * @var PersistenceManagerInterface
     */
    private $_persistenceManager;

    /**
     * @var SecurityInterface
     */
    private $_security;

    /**
     * @var RoutingInterface
     */
    private $_routing;

    /**
     * Constructor
     *
     * @param RequestInterface $request
     * @param TemplatingInterface $templating
     * @param PersistenceManagerInterface $persistenceManager
     * @param SecurityInterface $security
     * @param RoutingInterface $routing
     */
    public function __construct(RequestInterface $request,
        TemplatingInterface $templating,
        PersistenceManagerInterface $persistenceManager,
        SecurityInterface $security,
        RoutingInterface $routing
    ) {
        $this->_request = $request;
        $this->_templating = $templating;
        $this->_persistenceManager = $persistenceManager;
        $this->_security = $security;
        $this->_routing = $routing;
    }

    /**
     * Login form action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function loginAction()
    {
        return new Response(
            $this->_templating->render('User/login.html.php')
        );
    }

    /**
     * Process login form action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function processLoginAction()
    {
        // Try to get user
        $user = $this->_persistenceManager->findByColumn(
            'AshleyDawson\SimpleBlog\Model\User',
            'email',
            $this->_request->getRequestParameter('email')
        );

        // Does the user exist?
        if ($user instanceof User) {

            // Does the user have the correct password?
            if ($this->_security->isPasswordMatch($this->_request->getRequestParameter('password'), $user->password, $user->salt)) {

                // Create and set the current token
                $token = new SimpleAuthenticationToken();
                $token->setIsAuthenticated(true);
                $this->_security->setToken($token);

                // Redirect to admin section
                return new RedirectResponse(
                    $this->_routing->getRoute('admin_homepage')->getPath()
                );
            }
        }

        // Something went wrong
        return new Response(
            $this->_templating->render('User/login.html.php', array(
                'loginFailed' => true,
            )),
            401
        );
    }

    /**
     * Process logout
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function logoutAction()
    {
        // Destroy token
        $this->_security->destroyToken();

        // Redirect to admin section
        return new RedirectResponse(
            $this->_routing->getRoute('homepage')->getPath()
        );
    }
}