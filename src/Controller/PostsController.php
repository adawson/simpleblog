<?php

namespace AshleyDawson\SimpleBlog\Controller;

use AshleyDawson\SimpleFramework\Http\RequestInterface;
use AshleyDawson\SimpleFramework\Http\Response;
use AshleyDawson\SimpleFramework\Persistence\PersistenceManagerInterface;
use AshleyDawson\SimpleFramework\Templating\TemplatingInterface;

/**
 * Class PostsController
 *
 * @package AshleyDawson\SimpleBlog\Controller
 */
class PostsController
{
    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @var TemplatingInterface
     */
    private $_templating;

    /**
     * @var PersistenceManagerInterface
     */
    private $_persistenceManager;

    /**
     * Constructor
     *
     * @param RequestInterface $request
     * @param TemplatingInterface $templating
     * @param PersistenceManagerInterface $persistenceManager
     */
    public function __construct(RequestInterface $request, TemplatingInterface $templating,
        PersistenceManagerInterface $persistenceManager)
    {
        $this->_request = $request;
        $this->_templating = $templating;
        $this->_persistenceManager = $persistenceManager;
    }

    /**
     * List posts action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function listAction()
    {
        // Get the latest 5 posts, sorted in descending chronological order
        $posts = $this->_persistenceManager->findAll(
            'AshleyDawson\SimpleBlog\Model\Post',
             array(
                 'createdAt' => 'desc',
             ),
             0,
             5
        );

        // Return the response
        return new Response(
            $this->_templating->render('Posts/list.html.php', array(
                'posts' => $posts,
            ))
        );
    }

    /**
     * View post action
     *
     * @return \AshleyDawson\SimpleFramework\Http\ResponseInterface
     */
    public function viewAction()
    {
        $id = $this->_request->getQueryParameter('id');

        // Try to get the post by its PK
        $post = $this->_persistenceManager->find('AshleyDawson\SimpleBlog\Model\Post', $id);

        // If no post send a 404 response
        if ( ! $post) {
            return new Response(sprintf('Post with ID "%s" could not be found', $id), 404);
        }

        // Return the response
        return new Response(
            $this->_templating->render('Posts/view.html.php', array(
                'post' => $post,
            ))
        );
    }
}