<?php

// Define shorter directory separator constant
define('DIR_SEP', DIRECTORY_SEPARATOR);

// Basic PSR-4 autoloader
require_once __DIR__ . DIR_SEP . '..' . DIR_SEP . 'autoload.php';

// Create a request from globals
$request = \AshleyDawson\SimpleFramework\Http\RequestFactory::createFromGlobals();

// Boot application and handle request
$app = new \AshleyDawson\SimpleBlog\Application();
$response = $app->handle($request);

// Send the response
$response->send();