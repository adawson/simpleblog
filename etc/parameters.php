<?php

/**
 * Application global parameters
 *
 * @return array
 */
return array(

    // Database Parameters (MySQL)
    'db.default.host' => 'revopos.local',
    'db.default.username' => 'root',
    'db.default.password' => '',
    'db.default.port' => '3306',
    'db.default.database' => 'FrankRecruitmentBlog',
    'db.default.charset' => 'utf8',

    // ...

);