Simple Framework + Simple Blog
==============================

A (very) basic example of an MVC framework and blog implementation built as a demonstration within a day. This project
was built without using any external libraries or frameworks. Normally I'd use off-the-shelf components to build something like
this which would dramatically reduce the build time.

**View a [working demo](http://ec2-52-17-9-208.eu-west-1.compute.amazonaws.com/)** (Database is reset every 15 minutes).

Requirements
------------

```
PHP >= 5.4
MySQL >= 5
```

**Note: ** Tested on Mac OSX

Installation
------------

Clone the repository and move into the project directory.

```bash
$ git clone https://adawson@bitbucket.org/adawson/simpleblog.git
$ cd simpleblog
```

Configuration
-------------

Configure the application by removing the `.dist` from `etc/parameters.php.dist` and entering your own parameters.

```bash
$ cp etc/parameters.php.dist etc/parameters.php
$ vim etc/parameters.php
```

Install Schema/Load Fixtures
----------------------------

```bash
$ mysql -u <username> -p FrankRecruitmentBlog < src/Resources/schema/schema.sql
$ mysql -u <username> -p FrankRecruitmentBlog < src/Resources/schema/fixtures.sql
```

**Note:** The above example assumes that you've already created the database `FrankRecruitmentBlog`.

Start Server
------------

```bash
$ php -S localhost:8000 -t www/
```

Once the server is up and running you can go to the following URL in your browser:

```
http://localhost:8000/
```

Admin User
----------

The admin user included in the fixtures has the following login credentials:

* Email: joe.bloggs@example.com
* Password: Password1

Structure
---------

The structure of the project is in two parts, the framework and the domain. The framework files can be found in `/lib` and the domain files can be found in `/src`. The public (web) directory containing the front controller (index.php) can be found in `/www`.

Model structures in `src/Model` have a 1:1 column naming strategy with the DB table - so DB column names have camel case.

TODO
----

* Unit tests
* Functional tests
* Better HTML / Add styles
* Optimisation (Caching, etc.)