<?php

namespace AshleyDawson\SimpleFramework\DependencyInjection;

/**
 * Class Container
 *
 * @package AshleyDawson\SimpleFramework\DependencyInjection
 */
class Container implements ContainerInterface
{
    /**
     * @var array Service graph
     */
    private $_graph = array();

    /**
     * @var array Resolved service graph
     */
    private $_resolvedGraph = array();

    /**
     * {@inheritdoc}
     */
    public function register($name, \Closure $serviceResolver)
    {
        if (isset($this->_graph[$name])) {
            throw new \InvalidArgumentException(sprintf('Service already registered with name, "%s"', $name));
        }

        $this->_graph[$name] = $serviceResolver;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($name, $value)
    {
        if (isset($this->_graph[$name])) {
            throw new \InvalidArgumentException(sprintf('Parameter already registered with name, "%s"', $name));
        }

        $this->_graph[$name] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        if ( ! isset($this->_graph[$name])) {
            throw new \InvalidArgumentException(sprintf('Could not find service or parameter using name, "%s"', $name));
        }

        if (isset($this->_resolvedGraph[$name])) {
            return $this->_resolvedGraph[$name];
        }

        if ($this->_graph[$name] instanceof \Closure) {
            return $this->_resolvedGraph[$name] = $this->_graph[$name]($this);
        }

        return $this->_graph[$name];
    }
}