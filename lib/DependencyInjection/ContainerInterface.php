<?php

namespace AshleyDawson\SimpleFramework\DependencyInjection;

/**
 * Interface ContainerInterface
 *
 * @package AshleyDawson\SimpleFramework\DependencyInjection
 */
interface ContainerInterface
{
    /**
     * Register a service
     *
     * <code>
     * //Register a new service as a resolver
     * $container->register('my_service', function (ContainerInterface $c) {
     *     return new MyService($c->get('my_param_or_service'));
     * });
     * </code>
     *
     * @param string $name
     * @param callable $serviceResolver
     * @return $this
     */
    public function register($name, \Closure $serviceResolver);

    /**
     * Set a service container parameter
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setParameter($name, $value);

    /**
     * Get a service or parameter from the container
     *
     * @param string $name
     * @return object
     */
    public function get($name);
}