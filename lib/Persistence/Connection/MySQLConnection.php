<?php

namespace AshleyDawson\SimpleFramework\Persistence\Connection;

/**
 * Class MySQLConnection
 *
 * @package AshleyDawson\SimpleFramework\Persistence\Connection
 */
class MySQLConnection implements ConnectionInterface
{
    /**
     * @var \PDO
     */
    private $_connection;

    /**
     * Constructor
     *
     * @param string $host
     * @param string $username
     * @param string $password
     * @param int $port
     * @param string $database
     * @param string $charset
     */
    public function __construct($host, $username, $password, $port, $database, $charset)
    {
        $this->_connection = new \PDO(
            $this->_buildDsn($host, $port, $database, $charset),
            $username,
            $password
        );
    }

    /**
     * {@inheritdoc}
     */
    public function query($sql, array $parameters = array(), $fetchMode = self::FETCH_MODE_SINGLE)
    {
        $statement = $this->_connection->prepare($sql);

        $statement->execute($parameters);

        if (self::FETCH_MODE_SINGLE == $fetchMode) {
            return $statement->fetch();
        }
        elseif (self::FETCH_MODE_COLLECTION == $fetchMode) {
            return $statement->fetchAll();
        }

        throw new \RuntimeException(sprintf('Fetch mode "%s" is not recognised', $fetchMode));
    }

    /**
     * Build connection DSN
     *
     * @param string $host
     * @param int $port
     * @param string $database
     * @param string $charset
     * @return string
     */
    private function _buildDsn($host, $port, $database, $charset)
    {
        return sprintf('mysql:host=%s;port=%d;dbname=%s;charset=%s', $host, $port, $database, $charset);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastInsertId()
    {
        return current(
            $this->query('SELECT LAST_INSERT_ID()')
        );
    }
}