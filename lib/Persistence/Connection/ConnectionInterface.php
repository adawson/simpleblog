<?php

namespace AshleyDawson\SimpleFramework\Persistence\Connection;

/**
 * Interface ConnectionInterface
 *
 * @package AshleyDawson\SimpleFramework\Persistence\Connection
 */
interface ConnectionInterface
{
    /**
     * Query fetch modes
     */
    const FETCH_MODE_SINGLE = 'single';
    const FETCH_MODE_COLLECTION = 'collection';

    /**
     * Query the database and fetch wither a single assoc. array
     * or a collection
     *
     * @param string $sql
     * @param array $parameters
     * @param string $fetchMode
     * @return array
     */
    public function query($sql, array $parameters = array(), $fetchMode = self::FETCH_MODE_SINGLE);

    /**
     * Get last insert ID
     *
     * @return mixed
     */
    public function getLastInsertId();
}