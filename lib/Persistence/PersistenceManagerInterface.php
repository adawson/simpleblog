<?php

namespace AshleyDawson\SimpleFramework\Persistence;

/**
 * Interface PersistenceManagerInterface
 *
 * @package AshleyDawson\SimpleFramework\Persistence
 */
interface PersistenceManagerInterface
{
    /**
     * Persist a new model object
     *
     * @param object $model
     * @return void
     */
    public function persist($model);

    /**
     * Update an existing model object
     *
     * @param object $model
     * @return void
     */
    public function update($model);

    /**
     * Delete an existing model object by class and ID
     *
     * @param string $class
     * @param mixed $id
     * @return mixed
     */
    public function delete($class, $id);

    /**
     * Find a single model object
     *
     * @param string $class Fully qualified name of the model class
     * @param mixed $id
     * @return object
     */
    public function find($class, $id);

    /**
     * Find one model by a column value
     *
     * @param string $class
     * @param string $column
     * @param mixed $value
     * @return bool|mixed
     */
    public function findByColumn($class, $column, $value);

    /**
     * Find all model objects with soring and limiting
     *
     * @param string $class Fully qualified name of the model class
     * @param array $sort
     * @param int|null $offset
     * @param int|null $length
     * @return array
     */
    public function findAll($class, array $sort = array(), $offset = null, $length = null);
}