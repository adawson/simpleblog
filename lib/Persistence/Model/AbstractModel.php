<?php

namespace AshleyDawson\SimpleFramework\Persistence\Model;

/**
 * Class AbstractModel
 *
 * @package AshleyDawson\SimpleFramework\Persistence\Model
 */
abstract class AbstractModel
{
    /**
     * @var int
     */
    public $id;
}