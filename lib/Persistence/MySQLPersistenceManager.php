<?php

namespace AshleyDawson\SimpleFramework\Persistence;

use AshleyDawson\SimpleFramework\Persistence\Connection\ConnectionInterface;
use AshleyDawson\SimpleFramework\Persistence\Model\AbstractModel;

/**
 * Class MySQLPersistenceManager
 *
 * @package AshleyDawson\SimpleFramework\Persistence
 */
class MySQLPersistenceManager implements PersistenceManagerInterface
{
    /**
     * @var ConnectionInterface
     */
    private $_connection;

    /**
     * Constructor
     *
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->_connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function persist($model)
    {
        $this->assureModelStructure($model);

        // Compute table name and build query
        $class = get_class($model);
        $table = $this->computeTableName($class);

        $propertyNames = $this->_getModelPropertyNames($class);
        $columns = rtrim(implode(',', $propertyNames), ',');

        // Build placeholders
        $placeholders = '';
        foreach ($propertyNames as $propertyName) {
            $placeholders .= ":{$propertyName},";
        }
        $placeholders = rtrim($placeholders, ',');

        // Build query with placeholders
        $sql = "INSERT INTO `{$table}` ({$columns}) VALUES ({$placeholders})";

        // Run the query
        $this->_connection->query($sql, (array) $this->_transformModelTypesForPersistence($model));

        // Hydrate the ID (models must have id property)
        $model->id = $this->_connection->getLastInsertId();
    }

    /**
     * {@inheritdoc}
     */
    public function update($model)
    {
        $this->assureModelStructure($model);

        // Compute table name and build query
        $class = get_class($model);
        $table = $this->computeTableName($class);

        // Build placeholders
        $propertyNames = $this->_getModelPropertyNames($class);
        $pairs = '';
        foreach ($propertyNames as $propertyName) {
            $pairs .= "`$propertyName` = :{$propertyName},";
        }
        $pairs = rtrim($pairs, ',');

        // Build query with placeholders
        $sql = "UPDATE `{$table}` SET {$pairs} WHERE `id` = :id";

        // Run the query
        $this->_connection->query($sql, (array) $this->_transformModelTypesForPersistence($model));
    }

    /**
     * {@inheritdoc}
     */
    public function delete($class, $id)
    {
        $this->assureModelStructure($class);

        // Build query
        $table = $this->computeTableName($class);
        $sql = "DELETE FROM `$table` WHERE `id` = :id LIMIT 1";

        // Execute query
        $this->_connection->query($sql, array(
            ':id' => $id,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function find($class, $id)
    {
        // Build query
        $table = $this->computeTableName($class);
        $sql = "SELECT * FROM `$table` WHERE `id` = :id";

        // Execute query
        $row = $this->_connection->query($sql, array(
            ':id' => $id,
        ));

        if (false === $row) {
            return false;
        }

        return $this->hydrateNewModel($class, $row);
    }

    /**
     * {@inheritdoc}
     */
    public function findByColumn($class, $column, $value)
    {
        // Build query
        $table = $this->computeTableName($class);
        $sql = "SELECT * FROM `$table` WHERE `{$column}` = :value";

        // Execute query
        $row = $this->_connection->query($sql, array(
            ':value' => $value,
        ));

        if (false === $row) {
            return false;
        }

        return $this->hydrateNewModel($class, $row);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll($class, array $sort = array(), $offset = null, $length = null)
    {
        $collection = array();

        // Build initial query
        $table = $this->computeTableName($class);
        $sql = "SELECT * FROM `{$table}`";

        // Append sorting
        if (count($sort)) {
            $sql .= ' ORDER BY ';
            foreach ($sort as $column => $direction) {
                $direction = strtoupper($direction);
                $sql .= "`{$column}` {$direction},";
            }
            $sql = rtrim($sql, ',');
        }

        // Append Limiting
        if (is_int($offset) && is_int($length)) {
            $sql .= " LIMIT {$offset}, {$length}";
        }

        // Hydrate model objects
        foreach ($this->_connection->query($sql, array(), ConnectionInterface::FETCH_MODE_COLLECTION) as $row) {
            $collection[] = $this->hydrateNewModel($class, $row);
        }

        return $collection;
    }

    /**
     * Hydrate a new model object with DB data
     *
     * @param string $class
     * @param array $data
     * @return mixed
     */
    protected function hydrateNewModel($class, array $data)
    {
        $model = new $class();
        foreach ($data as $property => $value) {
            if (property_exists($class, $property)) {
                if (strtotime($value) && preg_match('/^[a-z0-9_]+At$/i', $property)) {
                    $model->$property = new \DateTime($value);
                }
                else {
                    $model->$property = $value;
                }
            }
        }
        return $model;
    }

    /**
     * Compute the table name based on the model class name. In this case it's the last
     * term of the fully qualified class namespace
     *
     * @param string $class
     * @return string
     */
    protected function computeTableName($class)
    {
        $class = trim($class);

        if (false !== strpos($class, '\\')) {
            return end(explode('\\', $class));
        }

        return $class;
    }

    /**
     * Get property names of model class
     *
     * @param string $class
     * @return array
     */
    private function _getModelPropertyNames($class)
    {
        $propertyNames = array();
        $reflectionClass = new \ReflectionClass($class);
        foreach ($reflectionClass->getProperties() as $property) {
            $propertyNames[] = $property->getName();
        }
        return $propertyNames;
    }

    /**
     * Transform model properties for persistence (i.e. transform \DateTime objects into strings)
     *
     * @param object $model
     * @return object Clone of model passed
     */
    private function _transformModelTypesForPersistence($model)
    {
        $newModel = clone $model;
        foreach ($this->_getModelPropertyNames(get_class($model)) as $property) {
            $value = $newModel->$property;
            if ($value instanceof \DateTime) {
                $newModel->$property = $value->format('Y-m-d H:i:s');
            }
        }
        return $newModel;
    }

    /**
     * Assure that the model type is extending the base model
     *
     * @param object|string $model Instance or class name
     * @return bool
     * @throws \UnexpectedValueException
     */
    protected function assureModelStructure($model)
    {
        if (is_string($model) && class_exists($model)) {
            $model = new $model();
        }

        if ( ! ($model instanceof AbstractModel)) {
            throw new \UnexpectedValueException(
                sprintf('Model "%s" does not extend the base model AshleyDawson\SimpleFramework\Persistence\Model\AbstractModel', get_class($model)));
        }
    }
}