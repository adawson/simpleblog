<?php

namespace AshleyDawson\SimpleFramework\Validation;

/**
 * Interface ValidationInterface
 *
 * @package AshleyDawson\SimpleFramework\Validation
 */
interface ValidationInterface
{
    /**
     * Validate a given model object, throw a violation
     * exception if validation fails
     *
     * @param object $model
     * @return void
     * @throws \AshleyDawson\SimpleFramework\Validation\Exception\ValidationViolationException
     */
    public function validate($model);

    /**
     * An array of validation rules (closures). E.g.
     *
     * <code>
     * public function getRules()
     * {
     *     return array(
     *         'title' => array(
     *             'message' => 'Title must not be empty',
     *             'validator' => function ($value) {
     *                 return ( ! empty($value))
     *             },
     *         ),
     *     );
     * }
     * </code>
     *
     * Note: the closure has the value of the property as its argument and
     * if it returns TRUE the property is deemed valid
     *
     * @return array
     */
    public function getRules();

    /**
     * Returns TRUE if the model passed to the @see validate() method
     * is supported
     *
     * @param object $model
     * @return bool
     */
    public function isModelSupported($model);
}