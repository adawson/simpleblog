<?php

namespace AshleyDawson\SimpleFramework\Validation;

use AshleyDawson\SimpleFramework\Validation\Exception\ValidationViolationException;

/**
 * Class AbstractValidation
 *
 * @package AshleyDawson\SimpleFramework\Validation
 */
abstract class AbstractValidation implements ValidationInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate($model)
    {
        // Check that model is actually supported by this validation
        if ( ! $this->isModelSupported($model)) {
            throw new \InvalidArgumentException(
                sprintf('Model object "%s" not supported by validator "%s"', get_class($model), get_called_class()));
        }

        $reflection = new \ReflectionObject($model);
        $rules = $this->getRules();
        $violations = array();

        // Iterate over each property and validate it
        foreach ($reflection->getProperties() as $property) {
            $propertyName = $property->getName();
            if (isset($rules[$propertyName])) {
                $rule = $rules[$propertyName];
                foreach ($rule as $validatorSet) {
                    $validator = $validatorSet['validator'];
                    if ( ! $validator($model->$propertyName)) {
                        $violations[$propertyName][] = $validatorSet['message'];
                    }
                }
            }
        }

        // If there are violations, throw exception
        if (count($violations)) {
            throw new ValidationViolationException($violations);
        }
    }
}