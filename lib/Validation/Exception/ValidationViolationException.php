<?php

namespace AshleyDawson\SimpleFramework\Validation\Exception;

/**
 * Class ValidationViolationException
 *
 * @package AshleyDawson\SimpleFramework\Validation\Exception
 */
class ValidationViolationException extends \UnexpectedValueException
{
    /**
     * @var array
     */
    private $_violations;

    /**
     * Constructor
     *
     * @param array $violations
     */
    public function __construct(array $violations = array())
    {
        $this->_violations = $violations;
        parent::__construct('Validation failed');
    }

    /**
     * Get a list of validation violation messages keyed
     * by property name
     *
     * @return array
     */
    public function getViolations()
    {
        return $this->_violations;
    }
}