<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Interface RequestInterface
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
interface RequestInterface
{
    /**
     * HTTP methods supported (limited as this is basic example)
     */
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    /**
     * Get a query parameter (i.e. from $_GET)
     *
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     */
    public function getQueryParameter($name, $default = null);

    /**
     * Get a request parameter (i.e. from $_POST)
     *
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     */
    public function getRequestParameter($name, $default = null);

    /**
     * Get a file (i.e. from $_FILES)
     *
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     */
    public function getFile($name, $default = null);

    /**
     * Get a server parameter (i.e. from $_SERVER)
     *
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     */
    public function getServerParameter($name, $default = null);

    /**
     * Get query parameters
     *
     * @return array
     */
    public function getQuery();

    /**
     * Get request parameters
     *
     * @return array
     */
    public function getRequest();

    /**
     * Get files
     *
     * @return array
     */
    public function getFiles();

    /**
     * Get server parameters
     *
     * @return array
     */
    public function getServer();

    /**
     * Get HTTP method
     *
     * @return string
     */
    public function getMethod();

    /**
     * Set a query parameter
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setQueryParameter($name, $value);
}