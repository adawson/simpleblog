<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Interface ResponseInterface
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
interface ResponseInterface
{
    /**
     * Send response, terminating the application
     *
     * @return void
     */
    public function send();
}