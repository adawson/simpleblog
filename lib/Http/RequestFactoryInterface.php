<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Interface RequestFactoryInterface
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
interface RequestFactoryInterface
{
    /**
     * Create a request instance from the PHP autoglobals
     *
     * @return RequestInterface
     */
    public static function createFromGlobals();
}