<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Class Response
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
class Response implements ResponseInterface
{
    /**
     * @var string
     */
    private $_content;

    /**
     * @var int
     */
    private $_statusCode;

    /**
     * @var array
     */
    private $_headers;

    /**
     * @var array Status code to text mapping (only contains a limited number as this is an example)
     */
    protected static $statusText = array(
        200 => 'OK',
        201 => 'Created',
        204 => 'No Content',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
    );

    /**
     * Constructor
     *
     * @param string $content
     * @param int $statusCode
     * @param array $headers
     */
    public function __construct($content, $statusCode = 200, array $headers = array())
    {
        $this->_content = $content;
        $this->_statusCode = $statusCode;
        $this->_headers = $headers;
    }

    /**
     * {@inheritdoc}
     */
    public function send()
    {
        // Set headers if not already sent
        if ( ! headers_sent()) {
            header(sprintf('HTTP/1.1 %s %s', $this->_statusCode, self::$statusText[$this->_statusCode]));
            foreach ($this->_headers as $name => $value) {
                header(sprintf('%s: %s', $name, $value));
            }
        }

        echo $this->_content;
    }
}