<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Class RedirectResponse
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
class RedirectResponse extends Response
{
    /**
     * Constructor
     *
     * @param string $path URL/URI to redirect to
     */
    public function __construct($path)
    {
        parent::__construct('', 302, array(
            'Location' => $path,
        ));
    }
}