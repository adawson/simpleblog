<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Class Request
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
class Request implements RequestInterface
{
    /**
     * @var array
     */
    private $_query;

    /**
     * @var array
     */
    private $_request;

    /**
     * @var array
     */
    private $_files;

    /**
     * @var array
     */
    private $_server;

    /**
     * Constructor
     *
     * @param array $query Usually from $_GET autoglobal
     * @param array $request Usually from $_POST autoglobal
     * @param array $files Usually from $_FILES autoglobal
     * @param array $server Usually from $_SERVER autoglobal
     */
    public function __construct(array $query, array $request, array $files, array $server)
    {
        $this->_query = $query;
        $this->_request = $request;
        $this->_files = $files;
        $this->_server = $server;
    }

    /**
     * {@inheritdoc}
     */
    public function getQueryParameter($name, $default = null)
    {
        return $this->_getParameter($this->_query, $name, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestParameter($name, $default = null)
    {
        return $this->_getParameter($this->_request, $name, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function getFile($name, $default = null)
    {
        return $this->_getParameter($this->_files, $name, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function getServerParameter($name, $default = null)
    {
        return $this->_getParameter($this->_server, $name, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * {@inheritdoc}
     */
    public function getFiles()
    {
        return $this->_files;
    }

    /**
     * {@inheritdoc}
     */
    public function getServer()
    {
        return $this->_server;
    }

    /**
     * Get a single parameter from an array of parameters. If parameter is not
     * found then $default is returned
     *
     * @param array $parameters
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    private function _getParameter(array $parameters, $name, $default)
    {
        if ( ! isset($parameters[$name])) {
            return $default;
        }

        return $parameters[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return strtoupper($this->getServerParameter('REQUEST_METHOD'));
    }

    /**
     * {@inheritdoc}
     */
    public function setQueryParameter($name, $value)
    {
        $this->_query[$name] = $value;

        return $this;
    }
}