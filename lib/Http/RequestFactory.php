<?php

namespace AshleyDawson\SimpleFramework\Http;

/**
 * Class RequestFactory
 *
 * @package AshleyDawson\SimpleFramework\Http
 */
class RequestFactory implements RequestFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public static function createFromGlobals()
    {
        return new Request(
            $_GET,
            $_POST,
            $_FILES,
            $_SERVER
        );
    }
}