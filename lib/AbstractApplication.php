<?php

namespace AshleyDawson\SimpleFramework;

use AshleyDawson\SimpleFramework\DependencyInjection\Container;
use AshleyDawson\SimpleFramework\DependencyInjection\ContainerInterface;
use AshleyDawson\SimpleFramework\Exception\ActionNotFoundException;
use AshleyDawson\SimpleFramework\Exception\ActionNotReturningResponseException;
use AshleyDawson\SimpleFramework\Exception\ControllerNotFoundException;
use AshleyDawson\SimpleFramework\Http\RequestInterface;
use AshleyDawson\SimpleFramework\Persistence\Connection\MySQLConnection;
use AshleyDawson\SimpleFramework\Persistence\MySQLPersistenceManager;
use AshleyDawson\SimpleFramework\Routing\RoutingFactory;
use AshleyDawson\SimpleFramework\Http\ResponseInterface;
use AshleyDawson\SimpleFramework\Security\Firewall;
use AshleyDawson\SimpleFramework\Security\Security;
use AshleyDawson\SimpleFramework\Templating\NativeTemplating;

/**
 * Class AbstractApplication
 *
 * @package AshleyDawson\SimpleFramework
 */
abstract class AbstractApplication extends Container
{
    /**
     * Handle a given request, translating it into a Response. This is the
     * entry point of the application in the front controller
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function handle(RequestInterface $request)
    {
        // Start the session
        session_start();

        // Inject parameters from config
        $this->injectParametersFromConfiguration();

        // Pass the master request to the service container
        $this->register('request', function (ContainerInterface $c) use ($request) {
            return $request;
        });

        // Inject default DB connection
        $this->injectDefaultDbConnection();

        // Inject default persistence manager
        $this->injectDefaultPersistenceManager();

        // Inject the configured routing service
        $this->injectRouting();

        // Inject templating
        $this->injectTemplating();

        // Register domain specific services/params
        $this->configureContainer();

        // Resolve routing
        $this->resolveRouting();

        // Inject security handler
        $this->injectSecurityHandler();

        // Inject firewall
        $this->injectFirewall();

        // Intercept request with firewall
        $this->get('firewall')->intercept($request);

        // Run controller action and return the response
        return $this->runControllerAction();
    }

    /**
     * Inject security handler
     *
     * @return void
     */
    protected function injectSecurityHandler()
    {
        $this->register('security', function (ContainerInterface $c) {
            return new Security();
        });
    }

    /**
     * Inject the default persistence manager
     *
     * @return void
     */
    protected function injectDefaultPersistenceManager()
    {
        $this->register('persistence_manager.default', function (ContainerInterface $c) {
            return new MySQLPersistenceManager($c->get('connection.default'));
        });
    }

    /**
     * Inject the default database connection
     *
     * @return void
     */
    protected function injectDefaultDbConnection()
    {
        $this->register('connection.default', function (ContainerInterface $c) {
            return new MySQLConnection(
                $c->get('db.default.host'),
                $c->get('db.default.username'),
                $c->get('db.default.password'),
                $c->get('db.default.port'),
                $c->get('db.default.database'),
                $c->get('db.default.charset')
            );
        });
    }

    /**
     * Inject templating into container
     *
     * @return void
     */
    protected function injectTemplating()
    {
        $this->register('templating', function (ContainerInterface $c) {
            return new NativeTemplating(
                $c->get('template_base_paths'),
                $c->get('routing'),
                $c->get('security')
            );
        });
    }

    /**
     * Try to run the controller's action for the current route
     *
     * @return ResponseInterface
     */
    protected function runControllerAction()
    {
        /** @var \AshleyDawson\SimpleFramework\Routing\RouteInterface $route */
        $route = $this->get('route');

        // Try to find controller
        try {
            $controller = $this->get(
                $route->getController()
            );
        }
        catch (\Exception $e) {
            throw new ControllerNotFoundException(
                sprintf('Controller service not found using name "%". Please register controller in service container', $route->getController()), 0, $e);
        }

        $actionMethodName = $route->getActionMethodName();

        // Does the action exist on the controller?
        if ( ! method_exists($controller, $actionMethodName)) {
            throw new ActionNotFoundException(
                sprintf('Action method "%s" could not be found on controller "%s"', $actionMethodName, get_class($controller)));
        }

        // Finally, run the controller action
        $response = $controller->$actionMethodName();

        // Sanity check the response
        if ( ! ($response instanceof ResponseInterface)) {
            throw new ActionNotReturningResponseException(
                sprintf('Action "%s" on controller "%s" did not return a valid AshleyDawson\SimpleFramework\Http\ResponseInterface', $route->getActionMethodName(), get_class($controller)));
        }

        return $response;
    }

    /**
     * Try to resolve the current route and pass to container
     *
     * @return void
     */
    protected function resolveRouting()
    {
        $this->register('route', function (ContainerInterface $c) {
            return $c->get('routing')->resolve();
        });
    }

    /**
     * Inject parameters from configuration into service container
     *
     * @return void
     */
    protected function injectParametersFromConfiguration()
    {
        $parameters = $this->_getConfigurationFromPath(
            $this->getConfigParametersPath()
        );

        // Inject parameters into service container
        foreach ($parameters as $name => $value) {
            $this->setParameter($name, $value);
        }
    }

    /**
     * Create the routing registry and inject into service container
     *
     * @return void
     */
    protected function injectRouting()
    {
        $this->register('routing_factory', function (ContainerInterface $c) {
            return new RoutingFactory(
                $c->get('request')
            );
        });

        $routingConfiguration = $this->_getConfigurationFromPath(
            $this->getRoutingConfigPath()
        );

        $this->register('routing', function (ContainerInterface $c) use ($routingConfiguration) {
            return $c
                ->get('routing_factory')
                ->createFromConfiguration($routingConfiguration)
            ;
        });
    }

    /**
     * Inject firewall into service container
     *
     * @return void
     */
    protected function injectFirewall()
    {
        $configuration = $this->_getConfigurationFromPath(
            $this->getSecurityConfigPath()
        );

        $this->register('firewall', function (ContainerInterface $c) use ($configuration) {
            return new Firewall(
                $configuration,
                $c->get('routing'),
                $c->get('security'),
                $c->get('route')
            );
        });
    }

    /**
     * Get and validate configuration from a path
     *
     * @param string $path
     * @return array
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    private function _getConfigurationFromPath($path)
    {
        if ( ! is_readable($path)) {
            throw new \RuntimeException(
                sprintf('Parameters file could not be found at location "%s"', $path));
        }

        $parameters = include $path;

        if ( ! is_array($parameters)) {
            throw new \UnexpectedValueException(sprintf('Parameters expected to be an array, %s given', gettype($parameters)));
        }

        return $parameters;
    }

    /**
     * Register any services/params within the service container. E.g.
     *
     * <code>
     * protected function configureContainer()
     * {
     *     $this
     *         ->register('my_service', function (ContainerInterface $c) {
     *             return new MyService($->get('my_parameter_or_service'));
     *         })
     *     ;
     * }
     * </code>
     *
     * @return void
     */
    protected function configureContainer()
    {
    }

    /**
     * Get the path of the parameters file
     *
     * @return string
     */
    abstract protected function getConfigParametersPath();

    /**
     * Get the path to the routing config file
     *
     * @return string
     */
    abstract protected function getRoutingConfigPath();

    /**
     * Get the path to the security/firewall configuration file
     *
     * @return string
     */
    abstract protected function getSecurityConfigPath();
}