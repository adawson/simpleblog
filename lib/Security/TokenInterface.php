<?php

namespace AshleyDawson\SimpleFramework\Security;

/**
 * Interface TokenInterface
 *
 * @package AshleyDawson\SimpleFramework\Security
 */
interface TokenInterface
{
    /**
     * Set is authenticated
     *
     * @param bool $isAuthenticated
     */
    public function setIsAuthenticated($isAuthenticated);

    /**
     * Is the current token authenticated?
     *
     * @return bool
     */
    public function isAuthenticated();
}