<?php

namespace AshleyDawson\SimpleFramework\Security;

/**
 * Class Security
 *
 * @package AshleyDawson\SimpleFramework\Security
 */
class Security implements SecurityInterface
{
    /**
     * Token session key
     */
    const SESSION_TOKEN_KEY = 'simple_framework.token';

    /**
     * {@inheritdoc}
     */
    public function setToken(TokenInterface $token)
    {
        $_SESSION[self::SESSION_TOKEN_KEY] = $token;
    }

    /**
     * {@inheritdoc}
     */
    public function getToken()
    {
        if (! isset($_SESSION[self::SESSION_TOKEN_KEY])) {
            return false;
        }

        return $_SESSION[self::SESSION_TOKEN_KEY];
    }

    /**
     * {@inheritdoc}
     */
    public function destroyToken()
    {
        unset($_SESSION[self::SESSION_TOKEN_KEY]);
    }

    /**
     * Does the password match the encoded version?
     *
     * @param string $passwordInClear
     * @param string $password
     * @param string $salt
     * @return bool
     */
    public function isPasswordMatch($passwordInClear, $password, $salt)
    {
        return ($password == sha1($salt . $passwordInClear));
    }
}