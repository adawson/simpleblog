<?php

namespace AshleyDawson\SimpleFramework\Security;

/**
 * Class SimpleAuthenticationToken
 *
 * @package AshleyDawson\SimpleFramework\Security
 */
class SimpleAuthenticationToken implements TokenInterface
{
    /**
     * @var bool
     */
    private $_isAuthenticated = false;

    /**
     * {@inheritdoc}
     */
    public function setIsAuthenticated($isAuthenticated)
    {
        $this->_isAuthenticated = $isAuthenticated;
    }

    /**
     * {@inheritdoc}
     */
    public function isAuthenticated()
    {
        return $this->_isAuthenticated;
    }
}