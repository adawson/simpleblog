<?php

namespace AshleyDawson\SimpleFramework\Security;

use AshleyDawson\SimpleFramework\Http\RequestInterface;

/**
 * Interface FirewallInterface
 *
 * @package AshleyDawson\SimpleFramework\Security
 */
interface FirewallInterface
{
    /**
     * Intercept a request and redirect to login if no authenticated
     * token detected for path prefix
     *
     * @param RequestInterface $request
     * @return void
     */
    public function intercept(RequestInterface $request);
}