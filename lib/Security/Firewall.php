<?php

namespace AshleyDawson\SimpleFramework\Security;

use AshleyDawson\SimpleFramework\Http\RedirectResponse;
use AshleyDawson\SimpleFramework\Http\RequestInterface;
use AshleyDawson\SimpleFramework\Routing\RouteInterface;
use AshleyDawson\SimpleFramework\Routing\RoutingInterface;

/**
 * Class Firewall
 *
 * @package AshleyDawson\SimpleFramework\Security
 */
class Firewall implements FirewallInterface
{
    /**
     * @var array
     */
    private $_configuration;

    /**
     * @var RoutingInterface
     */
    private $_routing;

    /**
     * @var SecurityInterface
     */
    private $_security;

    /**
     * @var RouteInterface
     */
    private $_currentRoute;

    /**
     * Constructor
     *
     * @param array $configuration
     * @param RoutingInterface $routing
     * @param SecurityInterface $security
     * @param RouteInterface $currentRoute
     */
    public function __construct(array $configuration, RoutingInterface $routing, SecurityInterface $security, RouteInterface $currentRoute)
    {
        $this->_configuration = $configuration;
        $this->_routing = $routing;
        $this->_security = $security;
        $this->_currentRoute = $currentRoute;
    }

    /**
     * {@inheritdoc}
     */
    public function intercept(RequestInterface $request)
    {
        $uri = $request->getServerParameter('REQUEST_URI');

        // Handle DMZ
        if (in_array($this->_currentRoute->getName(), array(
            $this->_configuration['login_route'],
            $this->_configuration['login_process_route'],
            $this->_configuration['logout_route'],
        ))) {
            return ;
        }

        // Are we in a protected zone?
        if (preg_match($this->_getPathPrefixMatchRegex(), $uri)) {

            // Do we have a valid, authentic token
            if (( ! $this->_security->getToken()) || ! $this->_security->getToken()->isAuthenticated()) {

                // Bad or no token, redirect to login
                $response = new RedirectResponse(
                    $this->_routing->getRoute($this->_configuration['login_route'])->getPath()
                );
                $response->send();

                // Stop dead
                exit;
            }
        }
    }

    /**
     * Get path prefix match regex
     *
     * @return string
     */
    private function _getPathPrefixMatchRegex()
    {
        $prefix = str_replace('/', '\\/', $this->_configuration['path_prefix']);

        return "/^$prefix.*$/i";
    }
}