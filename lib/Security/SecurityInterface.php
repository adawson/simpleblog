<?php

namespace AshleyDawson\SimpleFramework\Security;

/**
 * Interface SecurityInterface
 *
 * @package AshleyDawson\SimpleFramework\Security
 */
interface SecurityInterface
{
    /**
     * Set token
     *
     * @param TokenInterface $token
     */
    public function setToken(TokenInterface $token);

    /**
     * Get token
     *
     * @return TokenInterface|bool
     */
    public function getToken();

    /**
     * Destroys the current token
     *
     * @return void
     */
    public function destroyToken();

    /**
     * Does the password match the encoded version?
     *
     * @param string $passwordInClear
     * @param string $password
     * @param string $salt
     * @return bool
     */
    public function isPasswordMatch($passwordInClear, $password, $salt);
}