<?php

namespace AshleyDawson\SimpleFramework\Exception;

/**
 * Class ControllerNotFoundException
 *
 * @package AshleyDawson\SimpleFramework\Exception
 */
class ControllerNotFoundException extends \RuntimeException
{
}