<?php

namespace AshleyDawson\SimpleFramework\Exception;

/**
 * Class ActionNotReturningResponseException
 *
 * @package AshleyDawson\SimpleFramework\Exception
 */
class ActionNotReturningResponseException extends \UnexpectedValueException
{
}