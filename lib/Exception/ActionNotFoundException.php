<?php

namespace AshleyDawson\SimpleFramework\Exception;

/**
 * Class ActionNotFoundException
 *
 * @package AshleyDawson\SimpleFramework\Exception
 */
class ActionNotFoundException extends \RuntimeException
{
}