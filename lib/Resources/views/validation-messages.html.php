<?php if (count($violations)): ?>
    <ul class="errors">
        <?php foreach ($violations as $message): ?>
            <li><?php echo $message ?></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>