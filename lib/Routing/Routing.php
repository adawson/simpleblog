<?php

namespace AshleyDawson\SimpleFramework\Routing;

use AshleyDawson\SimpleFramework\Http\RequestInterface;
use AshleyDawson\SimpleFramework\Routing\Exception\RouteNotFoundException;

/**
 * Class Routing
 *
 * @package AshleyDawson\SimpleFramework\Routing
 */
class Routing implements RoutingInterface
{
    /**
     * @var RouteInterface[]
     */
    private $_routes;

    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * Construct
     *
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $this->_request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function addRoute(RouteInterface $route)
    {
        if (isset($this->_routes[$route->getName()])) {
            throw new \RuntimeException(sprintf('Route with name "%s" is already defined', $route->getName()));
        }

        $this->_routes[$route->getName()] = $route;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoute($name)
    {
        if ( ! isset($this->_routes[$name])) {
            throw new \InvalidArgumentException(sprintf('Route with name "%s" is not defined', $name));
        }

        return $this->_routes[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function getRoutes()
    {
        return $this->_routes;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve()
    {
        $uri = $this->_request->getServerParameter('REQUEST_URI');
        $method = $this->_request->getMethod();

        $matchedRoute = null;

        // Iterate over the route collection until we have a match
        foreach ($this->_routes as $route) {
            if (preg_match($route->getPathRegex(), $uri) && ($method == $route->getMethod())) {
                $matchedRoute = $route;
                break;
            }
        }

        // Bubble exception if route not found
        if (null === $matchedRoute) {
            throw new RouteNotFoundException(sprintf('Failed to find route for URI, "%s"', $uri));
        }

        // Bind to request to resolve path parameters, etc.
        $matchedRoute->bindToRequest($this->_request);

        return $matchedRoute;
    }
}