<?php

namespace AshleyDawson\SimpleFramework\Routing;

/**
 * Interface RoutingFactoryInterface
 *
 * @package AshleyDawson\SimpleFramework\Routing
 */
interface RoutingFactoryInterface
{
    /**
     * Create a routing service, hydrated with routes, from a
     * routing configuration array
     *
     * @param array $routingConfiguration
     * @return \AshleyDawson\SimpleFramework\Routing\RoutingInterface
     */
    public function createFromConfiguration(array $routingConfiguration);
}