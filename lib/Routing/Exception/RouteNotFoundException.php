<?php

namespace AshleyDawson\SimpleFramework\Routing\Exception;

/**
 * Class RouteNotFoundException
 *
 * @package AshleyDawson\SimpleFramework\Routing\Exception
 */
class RouteNotFoundException extends \RuntimeException
{
}