<?php

namespace AshleyDawson\SimpleFramework\Routing;
use AshleyDawson\SimpleFramework\Http\RequestInterface;

/**
 * Interface RouteInterface
 *
 * @package AshleyDawson\SimpleFramework\Routing
 */
interface RouteInterface
{
    /**
     * Get the unique name of this route
     *
     * @return string
     */
    public function getName();

    /**
     * get unresolved path
     *
     * @return string
     */
    public function getPath();

    /**
     * Get HTTP method
     *
     * @return string
     */
    public function getMethod();

    /**
     * Get route parameters (defined in path)
     *
     * @return array
     */
    public function getParameters();

    /**
     * Get controller (service name)
     *
     * @return string
     */
    public function getController();

    /**
     * Get action (method name prefix)
     *
     * @return string
     */
    public function getAction();

    /**
     * Get the controller action method name, usually like "fooAction()"
     *
     * @return string
     */
    public function getActionMethodName();

    /**
     * Get regular expression version of the route path including
     * parameters for resolution. E.g.
     *
     * Turns path: /posts/{id}/name/{name}
     * Into regex: /^\/posts\/([a-z0-9_-]+)\/name\/([a-z0-9_-]+)\/?$/i
     *
     * @return string
     */
    public function getPathRegex();

    /**
     * Bind this route to a request and try to resolve parameters
     *
     * @param RequestInterface $request
     * @return void
     */
    public function bindToRequest(RequestInterface $request);

    /**
     * Merge a collection of parameters with the route path
     *
     * @param array $parameters
     * @return string
     */
    public function mergePathWithParameters(array $parameters);
}