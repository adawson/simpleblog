<?php

namespace AshleyDawson\SimpleFramework\Routing;

use AshleyDawson\SimpleFramework\Http\RequestInterface;

/**
 * Class RoutingFactory
 *
 * @package AshleyDawson\SimpleFramework\Routing
 */
class RoutingFactory implements RoutingFactoryInterface
{
    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * Construct
     *
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $this->_request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function createFromConfiguration(array $routingConfiguration)
    {
        $routing = new Routing($this->_request);

        // Iterate over configuration, hydrating routing service
        foreach ($routingConfiguration as $name => $routeConfiguration) {
            $routing->addRoute(new Route(
                $name,
                $routeConfiguration['path'],
                $routeConfiguration['method'],
                $routeConfiguration['defaults']
            ));
        }

        return $routing;
    }
}