<?php

namespace AshleyDawson\SimpleFramework\Routing;

/**
 * Interface RoutingInterface
 *
 * @package AshleyDawson\SimpleFramework\Routing
 */
interface RoutingInterface
{
    /**
     * Add a route to routing service
     *
     * @param RouteInterface $route
     * @return $this
     * @throws \RuntimeException
     */
    public function addRoute(RouteInterface $route);

    /**
     * Get route by name
     *
     * @param string $name
     * @return RouteInterface
     * @throws \InvalidArgumentException
     */
    public function getRoute($name);

    /**
     * Get all routes
     *
     * @return RouteInterface[]
     */
    public function getRoutes();

    /**
     * Resolve the routing stack to a single route
     *
     * @return RouteInterface
     * @throws \AshleyDawson\SimpleFramework\Routing\Exception\RouteNotFoundException
     */
    public function resolve();
}