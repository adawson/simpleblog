<?php

namespace AshleyDawson\SimpleFramework\Routing;

use AshleyDawson\SimpleFramework\Http\RequestInterface;

/**
 * Class Route
 *
 * @package AshleyDawson\SimpleFramework\Routing
 */
class Route implements RouteInterface
{
    /**
     * Parameter matching regex
     */
    const PARAMETER_MATCH_REGEX = '/{([a-z_]+)}/i';

    /**
     * @var string
     */
    private $_name;

    /**
     * @var string
     */
    private $_path;

    /**
     * @var string
     */
    private $_method;

    /**
     * @var array
     */
    private $_parameters = array();

    /**
     * @var array
     */
    private $_defaults;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $path
     * @param string $method
     * @param array $defaults
     */
    public function __construct($name, $path, $method, array $defaults = array())
    {
        $this->_name = $name;
        $this->_path = $path;
        $this->_method = $method;
        $this->_defaults = $this->_parameters = $defaults;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters()
    {
        return $this->_parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getController()
    {
        return $this->_parameters['_controller'];
    }

    /**
     * {@inheritdoc}
     */
    public function getAction()
    {
        return $this->_parameters['_action'];
    }

    /**
     * {@inheritdoc}
     */
    public function getActionMethodName()
    {
        return $this->getAction() . 'Action';
    }

    /**
     * {@inheritdoc}
     */
    public function getPathRegex()
    {
        $path = rtrim($this->_path, '/');
        $path = preg_replace(self::PARAMETER_MATCH_REGEX, '([a-z0-9_-]+)', $path);
        $path = str_replace('/', '\\/', $path);

        return sprintf('/^%s\/?$/i', $path);
    }

    /**
     * {@inheritdoc}
     */
    public function bindToRequest(RequestInterface $request)
    {
        // Try to resolve route params using request
        $this->_resolveParameters($request);

        // Pass resolved parameters to request
        foreach ($this->_parameters as $name => $value) {
            $request->setQueryParameter($name, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function mergePathWithParameters(array $parameters)
    {
        $path = $this->getPath();
        foreach ($parameters as $name => $value) {
            $path = str_replace('{'.$name.'}', $value, $path);
        }
        return $path;
    }

    /**
     * Resolve route parameters from request
     *
     * @param RequestInterface $request
     */
    private function _resolveParameters(RequestInterface $request)
    {
        // Get URI values
        $uri = $request->getServerParameter('REQUEST_URI');
        preg_match($this->getPathRegex(), $uri, $values);

        // We don't need the whole URI
        unset($values[0]);

        // Rebase index
        $values = array_values($values);

        // Try to get the parameter names
        preg_match_all(self::PARAMETER_MATCH_REGEX, $this->_path, $parameterNames);

        // We don't need the whole path
        unset($parameterNames[0]);

        // Rebase index
        $parameterNames = array_values($parameterNames[1]);

        // Build parameters an assoc. array
        foreach ($parameterNames as $i => $name) {
            $this->_parameters[$name] = $values[$i];
        }
    }
}