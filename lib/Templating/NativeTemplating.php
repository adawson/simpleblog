<?php

namespace AshleyDawson\SimpleFramework\Templating;

use AshleyDawson\SimpleFramework\Routing\RoutingInterface;
use AshleyDawson\SimpleFramework\Security\SecurityInterface;
use AshleyDawson\SimpleFramework\Templating\Exception\TemplateNotFoundException;

/**
 * Class NativeTemplating
 *
 * @package AshleyDawson\SimpleFramework\Templating
 */
class NativeTemplating implements TemplatingInterface
{
    /**
     * @var array
     */
    private $_basePaths;

    /**
     * @var array
     */
    private $_blockContent = array();

    /**
     * @var string
     */
    private $_parentTemplate;

    /**
     * @var string
     */
    private $_currentBlockName;

    /**
     * @var RoutingInterface
     */
    private $_routing;

    /**
     * @var SecurityInterface
     */
    private $_security;

    /**
     * Constructor
     *
     * @param array $basePaths
     * @param RoutingInterface $routing
     * @param SecurityInterface $security
     */
    public function __construct(array $basePaths = array(), RoutingInterface $routing, SecurityInterface $security)
    {
        $this->_basePaths = $basePaths;
        $this->_routing = $routing;
        $this->_security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function addBasePath($path)
    {
        $this->_basePaths[$path] = $path;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render($template, array $context = array())
    {
        ob_start();
        
        extract($context);

        include $this->_getTemplatePath($template);

        if ($this->_parentTemplate) {
            include $this->_parentTemplate;
        }

        return ob_get_clean();
    }

    /**
     * Try to get the template path
     *
     * @param string $template
     * @return string
     * @throws TemplateNotFoundException
     */
    private function _getTemplatePath($template)
    {
        $template = $this->_changeToNativeDirectoryDelimiters($template);

        // Iterate over base paths until we find a template
        $fullPath = null;
        foreach ($this->_basePaths as $basePath) {
            $path = $basePath . DIR_SEP . $template;
            if (is_readable($path)) {
                $fullPath = $path;
                break;
            }
        }

        // No template? Throw exception
        if (null === $fullPath) {
            throw new TemplateNotFoundException(sprintf('Template "%s" could not be found', $template));
        }

        return $fullPath;
    }

    /**
     * Change to native directory delimiters
     *
     * @param string $path
     * @return string
     */
    private function _changeToNativeDirectoryDelimiters($path)
    {
        return str_replace('/', DIR_SEP, $path);
    }

    /**
     * Template helper - Extend another template
     *
     * @param string $template
     */
    protected function extends_template($template)
    {
        $this->_parentTemplate = $this->_getTemplatePath($template);
    }

    /**
     * Template helper - Get a block's content
     *
     * @param string $name
     * @return string|null
     */
    protected function get_block($name)
    {
        return isset($this->_blockContent[$name]) ? $this->_blockContent[$name] : null;
    }

    /**
     * Template helper - Start block contents
     *
     * @param string $name
     * @return void
     */
    protected function block($name)
    {
        $this->_currentBlockName = $name;
        ob_start();
    }

    /**
     * Template helper - End block contents
     *
     * @return void
     */
    protected function end_block()
    {
        $this->_blockContent[$this->_currentBlockName] = ob_get_contents();
        ob_end_clean();
    }

    /**
     * Template helper - Get the relative path for a route
     *
     * @param string $routeName
     * @param array $parameters
     * @return string
     */
    protected function relative_path($routeName, array $parameters = array())
    {
        return $this->_routing->getRoute($routeName)->mergePathWithParameters($parameters);
    }

    /**
     * Template helper - Truncate a given string
     *
     * @param string $text
     * @param int $length
     * @return string
     */
    protected function truncate($text, $length)
    {
        return substr($text, 0, $length);
    }

    /**
     * Template helper - Renders violation messages as a list
     *
     * @param array|null $violations
     * @param string $name Name of the field
     * @return string
     */
    protected function list_validation_messages($violations, $name)
    {
        $messages = '';

        if (isset($violations[$name])) {

            $violations = $violations[$name];

            if (is_array($violations) && count($violations)) {

                extract(array(
                    'violations' => $violations,
                ));

                ob_start();
                include __DIR__ . DIR_SEP . '..' . DIR_SEP . 'Resources' . DIR_SEP . 'views' . DIR_SEP . 'validation-messages.html.php';
                $messages = ob_get_contents();
                ob_end_clean();
            }
        }

        return $messages;
    }

    /**
     * Template helper - Get current security token
     *
     * @return \AshleyDawson\SimpleFramework\Security\TokenInterface|bool
     */
    protected function current_token()
    {
        return $this->_security->getToken();
    }

    /**
     * Template helper - Include a template
     *
     * @param string $template
     */
    protected function include_template($template)
    {
        include $this->_getTemplatePath($template);
    }
}