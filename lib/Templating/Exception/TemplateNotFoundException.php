<?php

namespace AshleyDawson\SimpleFramework\Templating\Exception;

/**
 * Class TemplateNotFoundException
 *
 * @package AshleyDawson\SimpleFramework\Templating\Exception
 */
class TemplateNotFoundException extends \RuntimeException
{
}