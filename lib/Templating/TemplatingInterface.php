<?php

namespace AshleyDawson\SimpleFramework\Templating;

/**
 * Interface TemplatingInterface
 *
 * @package AshleyDawson\SimpleFramework\Templating
 */
interface TemplatingInterface
{
    /**
     * Add a base template path
     *
     * @param string $path
     * @return $this
     */
    public function addBasePath($path);

    /**
     * Render a template using context parameters
     *
     * @param string $template
     * @param array $context
     * @return string
     */
    public function render($template, array $context = array());
}