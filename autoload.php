<?php

// Autoload namespace mappings for PSR-4 compliance
$autoloadNamespaceMappings = array(
    array(
        'prefix' => 'AshleyDawson\\SimpleFramework',
        'dir' => 'lib',
    ),
    array(
        'prefix' => 'AshleyDawson\\SimpleBlog',
        'dir' => 'src',
    ),
);

// Register a basic PSR compatible autoloader
spl_autoload_register(function ($class) use ($autoloadNamespaceMappings) {

    $dirPrefix = __DIR__ . DIR_SEP;

    foreach ($autoloadNamespaceMappings as $autoloadNamespaceMapping) {

        // Flip slashes in namespace to become directory delimiters
        $classPath =  $dirPrefix . str_replace(array(
                $autoloadNamespaceMapping['prefix'],
                '\\',
            ), array(
                $autoloadNamespaceMapping['dir'],
                DIR_SEP,
            ), $class);

        $classPath .= '.php';

        // Try to include class
        if (is_readable($classPath)) {
            include $classPath;
        }
    }
});